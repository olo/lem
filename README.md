Wywiad dla Telewizji Literackiej (TVL)
======

Nagranie
--------
https://youtu.be/37K-P77tZXo

Zapis treści
------------
{01:23} **[Grzegorz Braun][]: Zaczęliśmy od tego, co tu leży na biurku u pana.**

[Stanisław Lem][]: Tak.

{01:26} **Czemu pan nie pisze książek do czytania?**

Bo uważam, że każdy normalny człowiek przez pewien czas życia robi coś,
co jest jego zawodem, a potem można zmienić zawód. Zainteresowania słabną,
albo też okoliczności towarzyszące tej działalności mierzchną i po prostu
ja się przekonałem, że dużo z tego, co było czystą moją fantazją,
takim nieodpowiedzialnym _pławieniem się_ w fantasmagoriach, stało się
rzeczywistością, często dość silnie skarykaturowaną przez urealnienie.
To był jeden z powodów. A drugi był po prostu taki, że na świecie zaczęło
się dziać tyle ciekawych rzeczy, że wyścigi dalsze fantazji - czy też
wyobraźni - z frontem zachodzących zjawisk były dość daremne, to znaczy:
mnie się przestało po prostu _chcieć_ ścigać z tymi... i zacząłem czytać
więcej, sięgać do źródeł naukowych, uczciwie, par excellance naukowych.
No już język ezopowych aluzji stał się zbędny, walka typowo prowadzona,
prawda, z władzą stała się niepotrzebna. Pozostały pewne residua, np.
takie, jak podpisywanie jakichś deklaracji, których nikt nie słucha i
nie zwraca żadnej uwagi; jak [Miłosz][Czesław Miłosz]
prosił mnie, żebym z [Wiśką Szymborską][Wisława Szymborska]
podpisał dla Rosjan przeciwko wojnie czeczeńskiej, podpisałem, ale
równocześnie powiedziałem, że to nie będzie mieć żadnego znaczenia.
Więc są pewne czynności, które przestają wychodzić albo z mody, albo
stają się po prostu jałowe. Np. niechętnie pisałbym - dajmy na to - o
niesłychanej ekspansji kosmonautycznej, bo ja nie wierzę w to, żeby
Ziemia, kula ziemska, mogła swoim wysiłkiem skolonizować Marsa.
A to, co uważam za pozbawione głębszego znaczenia i sensu, jest...
uważam za... najrozsądniej jest milczeć, mówiąc krótko: milczenie jest
złotem.

{03:55} **Ale pan nie milczy. Pan pisze teksty, ale one udają felietony...**

To nie są felietony. Ja wiem, wszyscy się rzucili na te felietony -
to nieprawda. ["Mgnienie oka"][Okamgnienie]
napisałem po prostu jako konfrontację moich starych (sprzed 40 lat)
zwidów, wizji, fantazji, marzeń - z rzeczywistością.
I to było trochę takie jak... powiedzmy sobie... jak
się pisze przewodnik po górach (to wtedy napisałem). Opisałem Himalaje,
czy Alpy. Ale to nie znaczy, że oczekiwałem, że każdy, kto przeczyta,
wlezie na wszystkie szczyty. To było do wyboru. No ale nie przypuszczałem
z drugiej strony, że ludzie zaczną się pchać tam, gdzie jest najmniej
ciekawe miejsce albo też będą to wykorzystywali to w sposób, który jakoś
urąga... jakby to powiedzieć... nie chcę powiedzieć: dobremu wychowaniu
(to nie jest dobre określenie), ale że nastąpi tak silna komercjalizacja.
Ja myślałem o... Jak pisałem "doścignąć i prześcignąć naturę", miałem na
myśli oczywiście imitowanie zjawisk życiowych, tzn. że będziemy
dokonywać plagiatów zjawisk życiowych. I teraz mam na biurku ostatni
numer [American Scientist],
gdzie właśnie mówi się o tych mikromaszynach atomowych.
No - to już nie mogę o tym pisać, bo ja właściwie nie jestem
stuprocentowym pisarzem-realistą; muszę wybiegać w jakieś inne wymiary,
prawda? To jest jedno. A drugie jest takie, że właściwie szmal jest
naszą gwiazdą powszechnie przewodnią. Oraz ewentualnie jeszcze sprawa
politycznych antagonizmów, więc teraz Amerykanie razem z Żydami skonstruowali
super-laser do strącania rakiet nieprzyjacielskich. Ale to też nie...
To już jest. Więc o tym, co jest, to niech piszą inni.
A ja się zastanawiałem nad tą dalszą przyszłością.
I ona się teraz zamgliła bardzo wyraźnie, ponieważ jesteśmy, stoimy na
kolosalnej obrotnicy tak jak w parowozowni.
Nie wiemy, jak będzie wyglądała przyszłość.
Jeżeli będziemy, po prostu... poddamy się naciskom rozwijających się
samorzutnie technologii, jest to niebezpieczeństwo.

We wczorajszym numerze [International Herald Tribune]
jest takie pasmo fotografii przedstawiające po pierwsze:
kurę, która składa jajka zawierające lizozym
(to jest antybiotyk dla człowieka przydatny); po drugie: jest koza,
z której mleka można wyłowić specjalny rodzaj bardzo odpornej na
naciąganie, znaczy: wytrzymałej niezmiernie pajęczyny. Następnie...
jednym słowem przy pomocy wsadzania genów rozmaitym zwierzętom
wytwarzamy najdziwniejsze rzeczy i ja się trochę tego boję.
Jak długo to było szczerą fantazją, Jaś i Małgosia, prawda, piernikowy
dach u czarownicy, to to jest nieszkodliwe. Natomiast zobaczyć
prawdziwą chatkę, i otrzymać rozkaz zeżarcia całego dachu z pierników,
grozi tylko jakimiś skutkami szkodliwymi dla trawienia. Więc to nie jest...
rzeczywistość po prostu ma to do siebie... krótko mówiąc: ja to mówiłem
[Żakowskiemu][Jacek Żakowski],
że wszystko, co pociąga, to jest między wyciągniętą ręką i owocem.
A jak się już ten owoc ma, to albo się okazuje, że jest zgniły,
albo jest przejrzały, albo nawet już nie smakuje, albo chcemy czegoś
innego.

A w tej chwili mamy nadmierną podaż informacji. Np. ja osobiście
przyznaję się szczerze, bo jestem człowiekiem z natury okropnie okrutnym,
to ja bym tych facetów, którzy wymyślają wirusy typu [ILOVEYOU], częściowo
na ścieżki zdrowia puszczał, częściowo smagał, a częściowo kazałbym
każdemu wytatuować na czole napis "wróg ludzkości numer 1" i takie rzeczy.
Przecież to są straty setek milionów dolarów.
Niedawno czytałem o młodym człowieku, który z jakiegoś hotelowego pokoiku
usiłował - i prawie mu się udało - opanować główny komputer wielkiego
lotniskowca amerykańskiego! Przecież to nie jest zdro...
Po pierwsze, gdybym ja to opisał kiedyś był 30 lat temu, to wszyscy
powiedzieliby: ten Lem zwariował, to w ogóle jest głupie.
A teraz się okazuje, że to, co jest idiotyczne, jest w granicach
naszych możliwości. I to, że my wybieramy to, co głupsze, łatwiejsze,
po prostu to jakoś... tamuje język w gębie, prawda? Zniechęca. A natomiast
takie rzeczy jak ["Okamgnienie"][Okamgnienie]...
mnie chodziło przede wszystkim o pokazanie,
że cała historia ludzkości - 140 tysięcy lat mniej-więcej, od *homo
primigenius*, *homo neanderthalensis* itd. - aż do początku skoku od epoki
mniej-więcej paleolitycznej do technosferycznej - to jest jakieś 300 lat
mniej-więcej. To jest jedno drgnienie sekundy na geologicznym zegarze, który
umieściłem na wewnętrznej stronie tak zwanego front-opisu w książce.
O to mi chodziło! Więc chodziło mi przede wszystkim o to, że my żyjemy
w niesłychanym tempie nieprawdopodobnego przyspieszenia, i ze względu
na to jesteśmy troszeczkę w sytuacji człowieka, który skoczył (mniejsza o to,
z jakiego powodu) bez spadochronu z 50-piętrowego wieżowca, i w tej
chwili znajduje się koło 30 piętra. Ktoś się wychyla i pyta się:
no jak tam? On mówi: _na razie_ wszystko w porządku. I leci dalej.
Otóż my się znajdujemy w takiej sytuacji, tylko my nie zdajemy sobie
sprawy z tego, jakie potworne przyspieszenie nas - ludzi; nie to Polaków,
nie to Francuzów: wszystkich razem - unosi. Także my w pewnym sensie
mając coraz potężniejsze technologie, coraz słabiej kontrolujemy
kierunek, w jakim one zmierzają, prawda. Więc to jest to.

{10:33} **A ta sprzeczność, jaka zachodzi między traktowaniem przez pana ludzkości jako stada durniów...**

A to przesada (*śmiech*). Nie wszyscy, nie...

{10:47} **a tym, że pan jednak do tej ludzkości przemawia, i pan jest kaznodzieją.**

Muszę powiedzieć, że jedyną rzeczą, do której się mogę spokojnie przyznać,
jest to, pisząc od początku... tzn. od czasu, kiedy przestałem się
zajmować czysto komercjalną robotą dla utrzymania się jako ubogi repatriant
ze Lwowa i pisałem jakieś brednie dla - co tu nie powiedzieć - takiej
gazetki prywatnej, która została zlikwidowana - to właściwie było
wszystko spowodowane, no, po prostu chęcią, czy orientowaniem się
w możliwościach cywilizacyjnych. I miałem nadzieję, że stworzyłem
fikcyjną postać konstruktora jako człowieka zasadniczo głęboko racjonalnego.
No ale ludzkość się tak ładnie nie zachowuje; ani tak rozumnie, ani tak
przyzwoicie. I szczególnie drażniące były dla mnie takie wyskoki
[Fukuyamy][Francis Fukuyama] z jego końcem historii, prawda...
uważałem to za brednie...

{11:52} **Stop! Muszę powściągnąć pański gest szurania po mikrofonie.**

(*cięcie*)

{11:57} **Ale ja nie chcę o [Fukuyamie][Francis Fukuyama] teraz.**

A o kim?

{12:03} **Przecież nikt nie czytał. To pan powiedział, że jak... że po pierwsze:
nikt nic nie czyta, po drugie: jak czyta, to nic nie rozumie,
a jak rozumie, to nie zapamiętuje.**

Tak jest.

{12:11} **Więc: o [Fukuyamie][Francis Fukuyama] już nikt nie pamięta,
w ogóle nikt nie wie, kto to taki, prawda?**

On w dalszym ciągu jest... no, mniejsza z tym.

{12:17} **Tak, ale teraz jest mniej ważny niż postać fikcyjna z jakiegoś opowiadania.
Ja o panu bym chciał, to znaczy: jak to jest, że... znaczy, po pierwsze
chcę się dowiedzieć, czy był kiedykolwiek taki moment, w którym pan wierzył
w tego racjonalnego konstruktora; w którym pan głęboko w niego wierzył.**

No... w pewnym sensie trochę tak, bo jestem wychowany...

{12:39} **Stop! Przepraszam. Moje pytania są tutaj tylko pretekstem, więc proszę o...**

Tak, tak. Oczywiście, że ja od dziecka miałem takie racjonalne przekonanie,
że największymi i najzacniejszymi ludźmi są profesorowie uniwersytetów.
Jednym słowem nauka jest tą siłą przodującą i wiodącą.
I ze względu na to książki czytałem. [Norbert Wiener], [Albert Einstein],
prawda, i tak dalej, i tak dalej...
Wszystko wskazywało na to, że jest sporo takich ludzi, którym zależy na tym...
No [Norbert Wiener] napisał książkę
["Human use of human beings"][Human use of human beings]
o ludzkim postępowaniu z ludźmi.
Więc - to nie byli tylko cybernetycy ([Einstein][Albert Einstein] był
z kolei pacyfistą itd.); więc ja się rzeczywiście tym przejmowałem.
Na przykład ja przez całą okupację, jak byłem mechanikiem samochodowym,
wracałem o 8 rano do domu w tych rozmaitych brudnych olejach, nosiłem -
znaczy nie ze sobą, ale w domu miałem - jedną książkę tylko:
[Eddingtona][Arthur Eddington]
"O wewnętrznej budowie gwiazd"; dlatego po niemiecku, bo taką miałem.
I studiowałem z rozkoszą, ponieważ był to bardzo światły człowiek.
Aczkolwiek to jeszcze było w epoce, kiedy nie były znane nuklearne
źródła energii gwiazdowej. Więc ja istotnie - a poza wszystkim jeszcze
trzeba powiedzieć, że... jeszcze bardziej cofnąć się wstecz - odkąd
przestałem się zajmować zarobkową pisaniną, pisałem nie myśląc w ogóle
o żadnych czytelnikach. Tzn. jakby mnie kto wtedy przycisnął do muru,
powiedziałbym zapewne: chyba jest tak, że rzeczy, sprawy, które mnie
niezmiernie interesują i fascynują i pochłaniają, powinny jakoś innych
też zainteresować, ale nie było to wymierzone do jakichś konkretnych
osób, do jakichś postaci. I nigdy nie należałem do żadnej grupy
literackiej, nie byłem członkiem żadnej redakcji, żadnej partii
politycznej. Byłem sobie - jak mówi [Kipling][Rudyard Kipling] -
kot, który chadza własnymi drogami.

{14:47} **No ale jeżeli kiedyś pan wierzył w tego racjonalnego konstruktora,
kiedyś - prawda? - dawno, czytając o wewnętrznej budowie gwiazd,
no to już teraz zdążył się pan przekonać w to, że to jest postać literacka.
I teraz interesuje mnie ta sprzeczność, no że skoro już pan przekonał
się o tym, że to nie dość, że wszyscy idioci,
to jeszcze nie ma nawet prawdziwych konstruktorów,
to co pana korci jednak do tego, żeby przemawiać do ludu? Bo pan...
co to jest? To jakiś pisarz jest jednak w panu racjonalnym jest coś
nieracjonalnego.**

Nie, raczej chciałem rzeczywiście skonfrontować coś, co pisałem w 2 porcjach:
["Dialogi"][Dialogi], ["Summa technologiae"][Summa technologiae],
mniej-więcej lat 45-50 temu, z rzeczywistością
po prostu, żeby się przekonać, dokonać bilansu, jakie jest saldo, co mi się
udało, a czego mi się nie udało, i o ile rzeczywistość realizowała
w jakimś takim nieraz karykaturalnym, wręcz karykaturalnym
wykrzywieniu to, co myślałem. Np. ja myślałem o jakichś -
powiedzmy sobie - wspaniałych samoczynnych urządzeniach, które się dziś
nazywa (cała gałąź) *robotyka*. Ale żeby się masowo sprzedawały elektroniczne
pieski i kotki, to mi do głowy nie przychodziło. Przecież to jest dobre
dla dzieci do 10. roku życia, tymczasem to jest strasznie modne.
Więc ja nie rozumiem tej potrzeby. My mamy w domu psy, ale ja bym
w żadnej ilości psów nie zamienił na jednego takiego ceramicznego
bałwana, który chodzi, prawda, i udaje, że on szczeka itd. Więc to jest
właściwie jakaś degeneracja tego pomysłu, prawda?

{16:37} **Ale czy nie wszystkie genialne wynalazki są okupione masowością
i trywializacją? Wynalazek druku, który z jednej strony daje nam dostęp
do tego, do czego 500 lat temu dostęp mieli wybrani, a okupujemy to
masą chłamu w kioskach.**

No zapewne. [Wells][Herbert Wells]
opisywał walki balonów w rodzaju zeppelinów (jeszcze nie były pod nazwą
sterowiec), które ostrogami, tak jak żaglowce, rozpruwały sobie powłoki.
Tymczasem potem się mówiło: ach, ludzkość zjednoczy lotnictwo.
Tymczasem okazało się, że można bombardować.
[Ciołkowski][Konstantin Ciołkowski]
wierzył, że kosmonautyka nas zjednoczy; to się okazało,
co można zrobić z kosmonautyką. No więc większość technologii ma
w marzeniach świetlany awers, który jest zwłaszcza u prekursorów
jaśniejącą gwiazdą, i jest niestety rewers, który jest czarną -
znaczy: ponurą - rzeczywistością, dosyć, mniej lub bardziej, trywialną.
Np. pan [Benz][Carl Benz] wymyślił samochód,
[Ford][Henry Ford] wymyślił samochód.
I jak my dziś wyglądamy z tymi samochodami? Przecież główny problem to jest
postawić ten samochód. Nie: jechać, tylko jak wydobyć się z korka
i jak postawić tę cholerę gdzieś, i żeby to nas nie otruło, prawda?
A poza tym generalnie my podcinamy gałąź biosfery, na której my
siedzimy, i wszyscy mniej-więcej fachowcy o tym wiedzą, i nikt nie ma
na to żadnego właściwie jakiegoś lekarstwa.
Więc to nie jest sytuacja, w której... owszem: rola dla tak zwanej
*fantasy* - to się pisze o panu z jeziora, panu z bajora, jakieś takie
historie. Ale mnie to nie bawi. [Tolkien] jakiś tam,
pan czy pani pierścieni, bransoletki... ja nie wiem, o co chodzi,
bo ja nie mam czasu na czytanie takich rzeczy,
po prostu dlatego, że doba ma tylko 24 godziny.

{18:51} **A na czytanie czego ma pan czas?**

No to jest tak: [Scientific American], [American Scientist],
potem jest [Science et Vie],
potem jest [Wissenschaft],
potem jest [Priroda][Природа],
dochodzi jeszcze ukraiński [Vsesvit][Всесвіт]
(to jest częściowo tłumaczenie amerykańskie).
No, i właściwie tam występuje czołówka uczonych światowych, i mnie to
naprawdę zajmuje, tym bardziej, że ostatnio - ze względu na okropny ścisk,
który panuje nie tylko w środkach masowego transportu, ale także w nauce -
każdy stara się - na zasadzie takiej
[ryciny](https://sztuka.agraart.pl/licytacja/403/24715):
tłum, a ktoś mówi: "panie Boże, tu jestem!", prawda? -
coś wymyślić takiego, co jest zupełnie inne od wszystkich.
Jest taki jeden [biochemik][Peter Duesberg],
który twierdzi w Ameryce, że wirus, czy też *virus*, nie powoduje
zapaści immunologicznej;
ktoś inny twierdzi - no nie, o tym, że ludzie w Australii chodzą do góry
nogami, to już było od dawna wiadomo - ale teraz się szuka jakichś bakterii
po jakichś planetach, wydaje się setki milionów na jakieś sondy,
które rozbijają się po odwrotnej stronie Marsa.
Ludzie lubią robić rzeczy szalone. To jest główny nasz *esprit de corps*,
no tacy jesteśmy, no co zrobić? Ja taki akurat nie jestem.

{20:18} **A ta polska proza nowa?**

No, jest jej bardzo dużo, trzeba powiedzieć.

{20:23} **Mojego pytania nie ma w programie.**

No, pani [Manueli Gretkowskiej][Manuela Gretkowska]
"Namiętnik" wziąłem do ręki, ale natknąłem
się na męski narząd płciowy w stanie wzwodu na jednej z pierwszych stron
i jakoś mnie to... nie powiem, ja nie mam nic przeciwko opisywaniu, ale
nie przesadzajmy - to nie jest takie nowe. To już istnieje od ostatnich
400 tysięcy czy 800 tysięcy lat, więc opisywanie tego... To jest - mogę
powiedzieć - owszem, na okładce widzę dość przystojną dziewczynę o takiej
cienkiej szyi, jak kurczę ma, ale żeby czytać to wszystko, to ja sobie
na to nie mogę pozwolić. Jedyna rzecz, która mi się spodobała - ale to
nie jest kwestia wieku, oczywiście - to były "Sołowieckie zapiski"
[Wilka][Mariusz Wilk],
które były najpierw u [Giedroycia][Jerzy Giedroyc]
w [paryskiej "Kulturze"][Kultura],
a potem w książce. Ja bym dał mu [Nike].
No ale to jest rzecz nie w mojej gestii, ja nie mogę rozdzielać nagród.

{21:31} **A jeszcze pan tak pięknie mówił o języku w tej nowej polskiej prozie.**

No więc to są wysiłki zrozumiałe. To jeszcze zaczęło się za nieboszczyka
[Drzeżdżona][Jan Drzeżdżon], [Marka Słyka][Marek Słyk],
to oni pisali rozmaite szalone rzeczy i przepuszczali przez maszynkę
do mielenia mięsa po prostu prozę i to co wychodziło stamtąd, no ale 
*nomina sunt odiosa*.
Jest pewien czcigodny i starszy krytyk, który się nazywa [Henryk Bereza]
i któremu się to szalenie podoba. Ale on jest okropnie odosobniony.
Nie znam nikogo absolutnie, kto by to był w stanie od początku do końca
przeczytać, a on ma jednak za sobą wielkie osiągnięcie, bo on to
potrafi czytać, i on się tym rozkoszuje. Oczywiście mi ktoś mówi, że to
jest masochizm - no ja nie wiem, znaczy mi się nie wydaje, żeby to mogło
znaleźć czytelników, niestety. Ale są rozmaite studia i są granty, są
sponsorzy, którym jest wszystko jedno; a żeby odpisać od podatku pewną kwotę,
to on się zgodzi, żeby coś takiego wyszło. No tak jest. Nie - żeby proza
(nie poezja) mogła być na własnych nogach, to autor musi mieć jakieś
doświadczenie życiowe.
Nie w tym sensie trywialnym, że on opisuje to, co on przeżył, tak jak to
np. - ja wiem - w ["Zaklętych rewirach"][Zaklęte rewiry]
(zresztą bardzo dobrze) [Worcell][Henryk Worcell]
zrobił, ale w tym sensie, że jak mówi tak: krowa żre trawę, trawa jest zielona
i dla nas zupełnie niejadalna; natomiast można wydoić z niej mleko i zrobić
z niego (jak Francuzi zrobili) 600 rodzajów sera. Otóż nie można powiedzieć,
że krowa jest bezpośrednio odpowiedzialna za to, co z serem...

(*cięcie*)

{23:26} **I to jest... Beret miał na to oko i ucho, rzeczywiście taki jest
dramat sztucznego tworu, jakim jest inteligent polski, prawda? Inteligent -
co to jest takiego. Potem wymyślono nową... nowe takie stworzenie, które się
nazywa intelektualista, prawda? Nie bardzo wiadomo, co to jest takiego.**

Nie, nie wiadomo.

{23:47} **Niektórzy są na etacie, prawda, intelektualisty. I rzeczywiście jest
jakieś takie rozdarcie tutaj, że ksiądz - ale świecki - nie ma sankcji
żadnej, więc nikomu nie może tym kropidłem przyłożyć - prawda? Pan... pan...
to jest zbiór kazań na niedziele i święta, to "Okamgnienie", prawda?**

No nie wydaje mi się.

{24:10} **Ale równocześnie no żadnego ratunku nie ma w tych pana tekstach.
Nie ma niczego, ja nie widzę żadnego ratunku, bo przecież pan
rygorystycznie odcina wszelką metafizykę od tej fizyki.**

A tak, bo to są dwie różne sprawy.
Po pierwsze, uważam, że ani metafizyki, to znaczy: wiary, nie należy
podpierać empirią/nauką, ani nauki - wiarą. To znaczy: to są dwie
różne dziedziny. Nie można watować, nie można łatać religią dziur
naszej ignorancji czysto rzeczowej. To znaczy: na pytanie, jak jest
urządzony wszechświat, ile ma wymiarów, nie można szukać odpowiedzi
w Ewangelii, tak samo jak w Koranie zresztą. To są różne kierunki
zupełnie. Religia jest społecznie potrzebna. Bardzo dobre zakończenie
do kolejnej książki rozmów ze mną, które prowadzi redaktor
[Fiałkowski][Tomasz Fiałkowski],
napisał [Jarzębski][Jerzy Jarzębski].
On twierdzi, że ja, podając się za ateistę, w samej rzeczy nim nie jestem,
ponieważ ja, zrównując wszystkie poszukiwania religijne (czyli metafizyczne),
poszukując transcendencji, staram się jakoś ją znaleźć, ale nie znajduję
jej w żadnym poszczególnym wyznaniu - prawdopodobnie dlatego, bo mam
to poczucie silne, że gdybym się urodził - powiedzmy - w krajach islamu,
to byłbym otoczony przez wyznawców Mahometa i Allaha. A gdybym się urodził
w Indiach, to znowu jakieś tam Shivy i tak dalej. Więc jednym słowem:
jakkolwiek możemy powiedzieć, że nauka się myli, to jednak ona swoje
błędy potrafi do pewnego stopnia jakoś korygować i jest tylko jedna,
chociaż się fizycy oczywiście za łby wodzą. Ale wyznań jest wiele i każde
obiecuje, że jest jedynym. Więc tej jedyności - przyznaję się - ja nie
uznaję. Nie widzę żadnej takiej specjalnej jedyności, która każe nam
doprowadzić do tego, że musi nastąpić konwersja na daną wiarę z innej wiary.

{26:35} **Dobrze, ale w tej racjonalności - jeszcze raz powtórzę - najbardziej
nieracjonalne wydaje mi się to u pana, że dalej chce się panu gadać.
Że chce się panu do tej ludzkości przemawiać.**

Kiedy nie bardzo.
Ja się zawsze na ogół zajmowałem tym, że ja pisałem... znaczy właściwie
bezpośrednim odbiorcą tego, co pisałem, była ta stara maszyna do pisania.
A co się potem będzie działo dalej, to mało mnie zajmowało, zwłaszcza,
kiedy w czasie [stanu wojennego][stan wojenny] udało mi się uciec do
[Berlina Zachodniego][Berlin Zachodni]
i tam już mogłem swobodnie rozwinąć skrzydła i pisać to, co mi się chciało.
Jednym słowem: ja nie wybierałem sobie czytelników, ja nikogo nie chciałem
ani pouczać, ani nawracać; nie nauczać, tylko pisałem to, co było moją -
no powiedzmy - zabarwioną jakoś, czasami humorem, czasami brakiem humoru,
emanacją mojej wyobraźni, po prostu. Robiłem to, co potrafiłem.
Jak pan ma do czynienia na przykład z akrobacją parterową i widzi pan
piramidę Chinek, a na samym szczycie tej piramidy siedzi Chinka, która
nogami podrzuca 16 filiżanek, to trudno się zapytać ją, jaki jest
racjonalny sens podrzucania 16 filiżanek nogami tak, żeby one spadały
jej na pięty, prawda? Każdy powinien robić to, co umie, a mnie się
zdawało, że ja umiem pisać - przez pewien czas. Potem zaczęło mi się
wydawać, że już trochę słabiej umiem - i przestałem. To jest właściwe
podejście. Zszedłem z kortu!

{28:13} **To znaczy to, co teraz czytamy w "Okamgnieniu", to jest dla pana
poza nawiasem?**

To jest poza nawiasem literatury pięknej, która nie była pisana z asercją,
czyli, prawda, jak się w logice mówi: z pełnym przekonaniem, że tak jest,
jak piszę, tylko w której była silna domieszka fantazji, która się potem
sprawdziła.
Byłem trochę tak jak w sytuacji - jakby powiedzieć: w szklanej łodzi
podwodnej, niewidzialnej, w głębinach oceanu. Potem wody zaczęły opadać,
więc ja się nagle patrzę i widzę, że jestem z tą moją łodzią na
rzeczywistym gruncie wyschłym. To jest ta rzeczywistość: te wszystkie
internety, te rozmaite... no... potężne straszliwie konsorcja biotechniczne,
które przede wszystkim... a bo to mnie przeraziło: patentowanie
fragmentów ludzkiego genomu. Patentowanie rozumiane jako pozyskiwanie
praw własnościowych do tego, co jest samą esencją naszej niepowtarzalnej
biologicznej i psychologicznej jednorazowości. Więc to mnie zasmuciło
przede wszystkim, tak. Chociaż ja się spotykałem z takimi obawami
dawniej, wygłaszanymi przez fachowców. Że się to źle skończy.

{29:49} **Ale jednak w tym wywiadzie dzisiejszym znajduję wyraźnie
przez pana wypowiedziane takie przekonanie, że linia rozwoju cywilizacji
ludzkości jest wiecznie wznosząca; że - to pan używa, buduje pan,
parafrazuje zapewne takiego określenia, że nie można zagrzebać z powrotem
w piasek wynalazku, prawda?**

Nie, nie można.

{30:15} **No ale przecież znamy z historii cywilizacje umarłe - cywilizacje,
które przykrył dosłownie piach. Czy to nie powinno pana uspokajać, że
każdą piramidę można zagrzebać?**

Nie bardzo, dlatego, ponieważ technologia ma własności autokatalityczne,
to znaczy: jak się dokona jakiegoś odkrycia, i wdroży się -
tak jak Internet, to ono z kolei zaczyna pączkować następnymi
towarzyszącymi jakimiś, prawda? I potwierdzają się całe olbrzymie produkcyjne
rozlewiska, które przynoszą dochody. Potem dochodzi do sytuacji,
w której [Microsoft] usiłuje
zmonopolizować rynek elektroniczny, a rząd amerykański usiłuje go
rozciąć na 3 kawałki jak smoka. A [Bill Gates]
broni się oczywiście adwokatami za miliardy dolarów przeciwko temu.
Więc to są te już wywołane samą sprawnością i tą akceleracją postępu
ciężkie boje.

{31:24} **A po czyjej pan jest stronie? Po stronie smoka, czy św. Jerzego -
[Billa Clintona][Bill Clinton]?**

Nie mogę być po stronie smoka. Ale rozumiem, że po prostu są pewne
zjawiska, takie na przykład, jak rozpętanie mimowolne przez ludzi
ocieplenia klimatu, na które nie mamy żadnego wpływu. Znaczy:
wszyscy mogą narzekać. Ot, mamy suszę teraz. Ale to jest jedna z
wypadkowych. A w Ameryce są huragany, tajfuny, trąby powietrzne.
Na zjawiska przyrody, której jesteśmy tylko drobniusieńkim tylko jakimś
żyjątkiem, nie mamy żadnego wpływu.

{32:08} **No dobrze, ale to mnie ciekawi. Bo pan też się tutaj odrzeka od
diagnoz ekonomicznych. Mówi pan: o ekonomii nie wypowiadam się; nie mówię
o polityce i ekonomii. Ale widzi pan: tak, jak z jednej strony tak mnie
to nęka, gdzie pan chowa te metafizykę (w którą kieszeń), tak samo mnie
ciekawi to, jak to jest z pańskim podejściem do ekonomii i polityki.
Weźmy ten przykład [Billa Gatesa][Bill Gates] i [Microsoftu][Microsoft],
prawda? To znaczy co: jest pan za tym, żeby
państwo limitowało wolną konkurencję?**

Jestem przeciwny monopolizacji. Oczywiście, tak. Monopolizacja jest
przełożoną dziedziny działań gospodarczych odnogą totalitaryzmu politycznego.
Bo ten, kto jest wyłącznym, powiedzmy sobie, producentem na rynku,
może dyktować warunki i od niego zależy podaż i popyt się musi
dostosować - dlatego dobra jest konkurencja. Tak to uważam, rzeczywiście.
Oczywiście konkurencja nie powinna mieć charakteru terroryzmu podkładanego
bombami pod te zakłady pana [Billa Gatesa][Bill Gates] -
to nie, ale ja bym powiedział, że człowiek jest istotą,
która właściwie najlepiej działa i czuje się
w strefie umiarkowanej: klimatu, produkcji, zarobków. Czyli co: niedobrze
jest mieć jednej pary butów tylko, jeszcze gorzej być boso, niedobrze
mieć 10 tysięcy par butów. Niedobrze jest nie móc zjeść obiadu i
niedobrze jest, kiedy się musi zjeść codziennie 10 obiadów. Po prostu
jesteśmy teraz zalewani głównie falami informacji, które już się
przekształcają w rodzaj broni. Mówi się o tym, że przyszła wojna
będzie na informacje.

{34:16} **No tutaj zanim zaczęliśmy nagranie, pan tak mimochodem rzucił,
że nie ma trzeciej drogi, ale proszę przypomnieć sobie, że taką diagnozę,
że niedobrze jest, kiedy ktoś ma 10 tysięcy par butów, bardzo wielu
naszych bliźnich polityków zamienia - czy natychmiast wnioskuje z takiej
diagnozy, że muszą przyjść bolszewicy i zabrać te 10 tysięcy par butów.
Takie wnioski z takich diagnoz się wysnuwa. Jaki jest pana...**

Ja przeżyłem ładnych parę lat w [Związku][Związek Radziecki],
znaczy we Lwowie, który stał się sowiecki, a potem
[Polskę Ludową][Polska Ludowa],
to ja nie jestem amatorem tego wszystkiego, nie nie nie nie.
Na żadne partyjniactwo z jakimiś takimi imponderabiliami totalitarnymi
nikt mnie nabierze.
To są moje osobiste predylekcje i animozje po prostu. Lubię, żeby było
wol... owszem: na przykład ja nie lubię pornografii. Ale jestem przeciwny
pornografii tylko dla tych, którzy jej koniecznie - nie wiem po co -
potrzebują. To znaczy: jestem przeciwnikiem cenzury. Wiem na przykład,
że w Stanach Zjednoczonych, kiedy doprowadzono prawie do absurdu...
tam można najokropniejsze bluźnierstwa i świństwa wystawiać jako
eksponaty. Mnie się to osobiście nie podoba, ale również i zakazywanie
kategoryczne i nakładanie sankcji karnych na to też mi się nie podoba.
Tu są kolizje, prawda? Kolizje pewnych wartości.

{35:35} **No ale w takich Stanach Zjednoczonych, gdzie można wystawiać
różne świństwa, nie można oglądać w kiosku tutaj przy pana ulicy
tych rzeczy, które można oglądać w naszym kiosku, prawda? W Stanach
Zjednoczonych to niemożliwe. Jakoś to rozwiązali.**

No bo są tacy, którzy twierdzą - niegłupie osoby - że my nie mamy
demokracji, tylko tzw. demokratoid, tak jak się mówi, że to nie jest
prawdziwa skóra, tylko dermatoid, prawda? Więc my mamy taką częściową,
jeszcze niedopieczoną, _zakalcowatą_ demokrację; w środku jest taki
duży zakalec korupcyjno-układowo-znajomościowo-itd. No - jesteśmy na
dorobku dopiero. Tu złoty się kiwa. Niedobrze jest, kiedy złoty był
przybity gwoździami i tak, że za jednego dolara trzeba było dać 120 zł.
Ale niedobrze jest także, kiedy złoty może ulecieć gdzieś - diabli wiedzą
gdzie. Więc ja też powtarzam, wracam do tego, co mówiłem: jesteśmy
istotami, które najlepiej czują się w strefach umiarkowanych. Pewne
minimum jest nam potrzebne: komfortu, dachu nad głową, środków do życia
itd. A cała reszta jest luksusem. Obecnie wkraczamy w epokę niewątpliwie
cywilizacji nie tylko informacyjnej, ale także obrazkowej. To znaczy:
ja już słyszałem - i tutaj, w tym pokoju - mowy pogrzebowe nad tzw.
[galaktyką Gutenberga][Galaktyka Gutenberga]:
kto będzie się fatygował do czytania książek,
kiedy można se to obejrzeć, itd., a ja powiedziałem: myśli nie można
zamienić, wymienić na obrazki. To się nie da zrobić, więc zawsze będziemy
musieli mieć jakiś... Nie wszyscy w końcu, na miłość Boską, czytali
[Kanta][Immanuel Kant].
Ale jak ktoś lubi, niech sobie czyta. Nie mam nic przeciwko temu.

{37:54} **Przetrwają książki papierowe?**

Papier się prawdopodobnie ostatnie, ale teraz już jest taki papier,
na którym można nawet... może on służyć jako rodzaj ekranu telewizyjnego.
Tak, że można sobie wyobrazić wytapetowanie całego pokoju takimi tapetami,
które będą rodzajem ekranów telewizyjnych. Mnie osobiście napawa to
niesmakiem, a nawet może grozą. Ale co to jest, jakie to ma znaczenie, co
mnie, a co mnie nie napawa niesmakiem. Taki jest generalny trend, prawda?
Usprawniania. Np. teraz się mówi o tym: inteligencja. Ponieważ inteligencji
maszynom prawdziwie nie można wszczepić, to się rozszerzyło samo pojęcie
jak worek. Inteligenta pralka to jest taka, która rozróżnia kolorową
i białą bieliznę, i rodzaj proszku i temperaturę i obroty suszarki itd.
To jest inteligentna pralka. A inteligentna lodówka daje sobie sama
radę i nie trzeba jej rozmrażać, daje sobie sama radę z oczyszczaniem
itd. Ja bym powiedział, że to jest bardzo skromna inteligencja przypominająca
raczej instynkt muchy, która sobie oczyszcza nóżkami skrzydełka, prawda?
Jesteśmy jeszcze daleko od sporządzenia sztucznych insektów.
Ale one będą. Ja to nazywam synsekty. Będą, i w straszliwych ilościach,
jak szarańcza, będą napadały naszych wrogów. Więc to jest oczywiście
niby niewinne, ale niepokojące wyobrażenie, prawda? Ja myślę, że to jest
możliwe, i tak będzie.

{39:47} **Czy czuje pan obrzydzenie do innych wynalazków poza maszyną do pisania?**

Nie, obrzydzenia nie czuję - na miłość Boską: ja przecież mam tutaj
urządzenie elektroniczne do mierzenia ciśnienia krwi, a znowu w kuchni
jest jakiś taki robot - *robot*, zdaje się, to się nazywa. Telewizor też
jest potrzebny. Tu jest wentylator, bardzo wskazane to jest. Mam nawet
golarkę elektryczną, nawet mam trzy - dlatego, no bo one bardzo źle golą
wszystkie. To znaczy: to są rzeczy potrzebne, ale to są wszystko rzeczy,
bez których na ogół można się obejść.

{40:29} **A Internet?**

Ja się broniłem przed komputerem. Potem się poddałem. Potem broniłem
się przed tym... faxem. Potem się poddałem. Potem broniłem się
przed skanerem. I przed browserem. I broniłem się przed Internetem.
No i mam teraz cały sekretariat sekretarzy. No i rzeczywiście nie byłbym
w stanie bez niego i bez... teraz ja już nie potrafię, jak on pójdzie
do domu, ja nie potrafię znaleźć listu, ja nic... za dużo tego jest,
po prostu.

{41:07} **Ale sekretarz, jak zauważyłem, to humanoid?**

Nie, to jest zwyczajny człowiek.
Ja nie wierzę andro... nie nie, nie nie nie, nie. Ja w ogóle uważam, że
kosztem jakichś dwóch, trzech miliardów dolarów dałoby się sporządzić
tak zwanego *butlera* - rodzaj lokaja w takiej kamizelce. Ale przecież
zwyczajna dziewczyna w białym fartuszku - to by było o wiele tańsze,
to nie kosztuje miliardów dolarów. Po jakiego dydka komuś jakieś takie
elektroniczne urządzenie, prawda? Które jeszcze do tego może się
odwinąć i dać w łeb. No więc nie; ja uważam, że trzeba być ostrożny
z tym automatyzowaniem wszystkiego.

{41:52} **Na marginesie: w jednym z programów Telewizyjnych Wiadomości
Literackich chcemy mówić o używkach - używając eufemizmu - a przede
wszystkim wódce: w literaturze i pisarstwie. Czy pan może w tej
sprawie się wypowiedzieć?**

Mogę.
Bardzo dawno temu, kiedy nie mieszkałem jeszcze w tym oto domu, tylko
w mniejszym przy tej samej ulicy, i pisałem mniej-więcej, znaczy
wstawałem, zaczynałem o piątej rano, a pisałem, aż dopóki nie tyle głowa,
tylko to odwrotne miejsce, na którym się siedzi, słabło (znaczy:
nie spadałem z krzesła); ale od czasu do czasu wstawałem, otwierałem szafkę
i pociągałem łyk. Najchętniej piłem - to jest bardzo poniżające mnie -
[Chartreuse];
bardzo lubiłem zieloną [Chartreuse].
A raz zszedłem do piwnicy i nagle zobaczyłem, że moja świętej pamięci
teściowa całą piwnicę wzdłuż regałów zapełniła pustymi flaszkami
po wypitych [Chartreuse]. To były przerażające ilości!
Przerażające, muszę powiedzieć.
I to, że ja właściwie dotrwałem po wypiciu takich ilości tych słodkich
alkoholi, jest zdumiewające.
A poza tym poddawałem się eksperymentom, to znaczy: byłem poddawany
działaniu psylocybiny - to jest taki halucynogen - pod kontrolą
psychiatryczną. To ciekawe było. Ale również ciekawe to, że jak to
minęło, i nie miałem żadnych później przykrych fizjologicznych
następstw, ale odmówiłem, kiedy mi zaproponowano drugi taki seans.
Potem jakaś ekipa, zdaje się - chyba z [Jugosławii][Jugosławia], przywiozła mi
kiedyś paczkę jakichś takich - co to było? - te: konopie jakieś takie
są, prawda. To te papierosy leżały u mnie w słoiku i rozsypały się.
Nigdy nie odczuwałem potrzeby zapalenia. Po prostu to, co mam w głowie,
to *quantum* imaginacji wystarczy mi w zupełności. Ja nie potrzebuję
po prostu żadnych dodatkowych... coś innego było z tą wódką. Ona nie
dodawała mi fantazji, tylko ona mi smakowała. Ja wiem, że prawdziwy
pijak wypije wodę brzozową, a nawet - jak będzie rozcieńczona - może
wypić trochę benzyny; bo jemu chodzi o to, żeby chlusnąć, prawda:
chluśniem, bo uśniem. Ale to jest mi zupełnie obce. Teraz już tylko
barszcz jest straszliwą uciechą, która... nie mogę.

{44:57} **Jeszcze jedna rzecz w tej kwestii:**

Słucham.

{44:58} **[Ernest Hemingway] podobno uważał pisanie na kacu za
sprawdzian męskości. Jeśli... czy to...
ma pan jakieś doświadczenia w tej materii?**

No nie wiem, trudno mi powiedzieć, bo ja nigdy nie piłem tyle, żeby
się upić; ja mam raczej mocną głowę. Mogę powiedzieć, że kiedy byłem
w Moskwie, byłem przyjmowany przez braci [Strugackich][bracia Strugaccy],
kolegów po fachu, to oni usiłowali mnie zwalić z nóg przy pomocy gruzińskiego
koniaku. I jak się jedna flaszka opróżniała, to nastąpiło jakieś takie
zeschematowanie i już była następna pełna na stole. Ja się nie dałem.
Ich było dwóch - ja byłem sam jeden. Ale to znaczy: ja wcale nie
zabierałem się za pisanie - myśmy rozmawiali. Myśmy *gawarili pa ruski*.
Ja znam dobrze rosyjski. Więc chcę powiedzieć, że w takich pojedynkach
wychodziłem dobrze, ale nie chciałem być, nie zależało mi na losie
alkoholika, bo to nie jest miły los. Nie mówiąc oczywiście o wątrobie,
ale nie warto. Człowiek powinien potrafić bez żadnych używek i bez
dopingu - czy to alkoholowego, czy jakiegoś kortykosteroidowego -
robić to, do czego jest stworzony; więcej nie potrzeba.

{46:28} **Wspomniał pan braci Strugackich...**

Ta.

{46:34} **Oni - i kto jeszcze liczy się? Mówi pan: koledzy po fachu.**

No to tylko był [Dick][Philip Dick]
bardzo dobry. On niestety nie żyje. On znowu zanadto dużo używał
niestety narkotyków.
Ale z tej branży to ja się raczej trzymam klasyków. O: [Wells][Herbert Wells],
[Stapledon][Olaf Stapledon],
no, to są ci... no, z naszych - [Żuławski][Jerzy Żuławski], oczywiście.
Dla mnie było wielkim rozczarowaniem dowiedzieć się i zobaczyć na własne
oczy, że powierzchnia księżyca jest taka łagodnie obła, i tam nie ma
tych strasznych zerwisk, tych urwisk straszliwych, które opisywał
w pierwszym tomie swojej trylogii Żuławski. Ale prawdę powiedziawszy,
jak byłem młody, to czytałem wszystko, bo ja chodziłem do pewnej
lwowskiej wypożyczalni i brałem książki tak jak stały na półkach,
po kolei. To było mniej-więcej tak: a to [Karol Maj],
a tu [Pitigrilli], a tu właśnie [Eddington][Arthur Eddington];
a potem zacząłem być coraz bardziej przebierny. I potem się przykleiłem
do spraw naukowych i na to już nie ma rady - nie mogłem się wycofać.

{47:52} **Taką mam osobistą prośbę: ja panu zawdzięczam kontakt z
[Philipem Dickiem][Philip Dick] - bardzo spóźniony, bo już byłem starym koniem, jak
zacząłem czytać [Philipa Dicka][Philip Dick], bo wcześniej bardzo pogardzałem
science fiction. I czy pan mógłby ponamawiać tych, co to jeszcze
nie rozumieją, że to jest warte czytania. Czy mógłby pan ponamawiać
na [Philipa Dicka][Philip Dick]?**

Mi się zdaje, że nawet towarzysz [Stalin]
razem z całą swoją kliką nie potrafił doprowadzić do sytuacji,
w której ludzie musieli czytać to, co im on kazał.
O żadnym przymusie mowy być nie może. Ludzie czytają tylko wtedy,
kiedy chcą. Ja nie wiem... owszem, akurat tak się złożyło,
że [mój syn][Tomasz Lem] tłumaczy teraz jedną -
bo on skończył fizykę teoretyczną w [Princeton] -
tłumaczy teraz jedną z późniejszych powieści [Dicka][Philip Dick].
Ale ja się nie podejmuję chodzić po ulicach z transparentem
"Czytajcie Dicka", prawda? No bo to nie jest skuteczne.
Jak ktoś nie lubi, to nie będzie.

{48:55} **A co w tym jest? Tak wyciągam pana i podpuszczam, żeby pan
coś ciepłego o nim powiedział.**

No on po prostu był niezmiernie oryginalną umysłowością, tylko on był
bardzo nierówny. On dawał rzeczy świetne mimo takich zapaści, po prostu.
To było wywołane jego osobistą biografią, która była niewesoła.
On np. przez pewien czas podejrzewał - bo my się znaliśmy
(korespondencyjnie) - że ja jestem starym agentem KGB. I że ja razem
z moim agentem wiedeńskim i jeszcze z profesorem, jednym Amerykaninem,
stanowimy taki trójkąt, który dąży do rzucenia Stanów Zjednoczonych
na kolana. Ale to było już w jego późnych latach, kiedy on miał trochę
przekręcone w głowie. Ja naprawdę nie wierzę, żeby KGB...

{49:51} **Czy te listy są? Ta korespondencja z [Dickiem][Philip Dick]? On pisał coś takiego?**

No cała... Mam taką gdzieś, nie wiem gdzie, taką ogromną stertę listów,
tak.

{50:02} **Od [Dicka][Philip Dick]?**

Nie tylko do mnie, przede wszystkim do jego znajomych, i do jego...
częściowo do tych, którzy usiłowali mu wyperswadować, że LEM nie jest -
jak to się mówi - trójcą KGB-owską, prawda? Bo on sądził, że -
ponieważ ja piszę w różnych stylach - to mnie musi być wielu.
I że ja się w ten sposób maskuję. Nie, to były z jego strony takie
zwidy. Nie, nie, nie, to, to... Ale poza tym są takie nazwiska...

Ostatnio leżałem u [prof. Szczeklika][Andrzej Szczeklik]
z moją niedomogą serca na klinice i czytałem po francusku [Simenona][Georges Simenon] -
po pierwsze dlatego, żeby sobie przypomnieć francuski (jak miałem 6 lat,
to miałem guwernantkę), a po drugie dlatego, że on był dobrym pisarzem.
Ale właściwie jego epoka minęła. To jest tak, że pisarze, nawet i nieźli,
przychodzą i odchodzą. Mało kto może się oprzeć tej
nieuchronnej erozji, którą czas jakoś wtłacza w niebyt poszczególne
dzieła i poszczególnych autorów. To jest bardzo smutne, ale na frontonie
[lwowskiej politechniki][Politechnika Lwowska] był napis:
*Hic mortui vivunt*.
Po to, żeby książka ożyła, zmartwychwstała, musi być czytelnik - bez niego
książka jest niczym, stosem zadrukowanego papieru. I tak jest ze wszystkim
na ogół. A te najwybitniejsze dzieła, o których się mówi, że to takie
słynne, że [Homer], że [Gilgamesz]...
Kto czyta [Homera][Homer] dzisiaj? Kto czyta [Gilgamesza][Gilgamesz]?
To się tylko wykłada na uniwersytecie, prawda.
To znaczy: zawsze *en vogue*, czyli w modzie są pewne... teraz jest taka pani
[Chmielewska][Joanna Chmielewska]
(nie czytałem), jak żyć z mężczyzną, jak wytrzymać z kobietą,
także pani (wspomniałem o niej) [Gretkowska][Manuela Gretkowska],
jest pani [Nurowska][Maria Nurowska];
w ogóle feminizm poszedł bardzo do przodu, ale to nie jest... ja...
dzień ma tylko 24 godziny. Kilka godzin trzeba jednak spać. A poza tym
ja teraz zawsze niestety jak człowiek, który był zagłodzony, i potem
w epoce pewnego dosytu zaczyna gromadzić wiktuały, tak i ja mam więcej
ściągam na moją biedną głowę książek i pism naukowych, niż jestem w stanie
pochłonąć. Dlatego tutaj leżą numery pism sprzed tygodni, które ja
ledwo otworzyłem; ja tylko się patrzę ze strachem, czy broń Boże jakieś
znowu odkrycie nie zostało dokonane, bo teraz nauka polega nie tylko na
tym, że trzeba się nauczyć, ale trzeba się przeuczać. Że to, co było
przed dwoma laty bardzo ważne i istotne, teraz już nie.
[Stephen Hawking]
napisał "Krótką historię czasu", a w niej jest takie zdanie:
"Moim zdaniem czas nie ma początku." I on dodał: "Przykro mi, że
zrobię tymi słowami... niestety sprawię przykrość papieżowi."
No więc ja sądzę, że papież się tym specjalnie nie przejmie, ale
jest ogromny jakiś taki rozplen tych teorii i hipotez naukowych
po prostu dlatego, że jest coraz więcej uczonych i każdy chce jakoś
rozbłysnąć, jak gwiazda - ta Westa, a niestety spadające gwiazdy
błysnąwszy - gasną. I taki jest los olbrzymiej większości utworów
literackich. A jeżeli coś jest np. tak jak w moim przypadku objawem
jakiegoś (mimowolnego!) prekursorstwa, to potem, jak to już wszyscy o tym
wiedzą: "a to ci Amerykę odkrył" - prawda?
Bo to już od dawna wiadomo, że na pewno teraz to już nie jest to...
Klonowanie? No to jest taka owca, Dolly się nazywa, prawda,
i nie ma o czym mówić w ogóle.
Na razie klonuje się myszy, potem się będzie klonowało ludzi.
Ale to nie wolno; ale to się przyzwyczają, itd. Więc mnie ten potok
niepokoi i staram się nie współdziałać w rozpędywaniu tej, prawda,
przybierającej informacyjnej fali, która zmierza do jakiegoś potopu.
Ale *nec Hercules contra plures*. Nic na to poradzić nie mogę.

{54:56} **Stop.**

[Grzegorz Braun]: https://pl.wikipedia.org/wiki/Grzegorz_Braun
[Stanisław Lem]: https://pl.wikipedia.org/wiki/Stanisław_Lem
[Czesław Miłosz]: https://pl.wikipedia.org/wiki/Czesław_Miłosz
[Wisława Szymborska]: https://pl.wikipedia.org/wiki/Wisława_Szymborska
[Okamgnienie]: https://pl.wikipedia.org/wiki/Okamgnienie
[American Scientist]: https://pl.wikipedia.org/wiki/American_Scientist
[International Herald Tribune]: https://en.wikipedia.org/wiki/International_Herald_Tribune
[Jacek Żakowski]: https://pl.wikipedia.org/wiki/Jacek_Żakowski
[ILOVEYOU]: https://pl.wikipedia.org/wiki/ILOVEYOU
[Francis Fukuyama]: https://pl.wikipedia.org/wiki/Francis_Fukuyama
[Norbert Wiener]: https://pl.wikipedia.org/wiki/Norbert_Wiener
[Albert Einstein]: https://pl.wikipedia.org/wiki/Albert_Einstein
[Human use of human beings]: https://en.wikipedia.org/wiki/The_Human_Use_of_Human_Beings
[Arthur Eddington]: https://pl.wikipedia.org/wiki/Arthur_Stanley_Eddington
[Rudyard Kipling]: https://en.wikipedia.org/wiki/Rudyard_Kipling
[Dialogi]: https://pl.wikipedia.org/wiki/Dialogi
[Summa technologiae]: https://pl.wikipedia.org/wiki/Summa_technologiae
[Herbert Wells]: https://pl.wikipedia.org/wiki/Herbert_George_Wells
[Konstantin Ciołkowski]: https://pl.wikipedia.org/wiki/Konstantin_Ciołkowski
[Carl Benz]: https://pl.wikipedia.org/wiki/Carl_Benz
[Henry Ford]: https://pl.wikipedia.org/wiki/Henry_Ford
[Tolkien]: https://pl.wikipedia.org/wiki/J.R.R._Tolkien
[Scientific American]: https://pl.wikipedia.org/wiki/Scientific_American
[American Scientist]: https://pl.wikipedia.org/wiki/American_Scientist
[Science et Vie]: https://en.wikipedia.org/wiki/Science_et_Vie
[Wissenschaft]: https://de.wikipedia.org/wiki/Bild_der_Wissenschaft
[Природа]: http://www.ras.ru/publishing/nature.aspx
[Всесвіт]: https://en.wikipedia.org/wiki/Vsesvit
[Peter Duesberg]: https://pl.wikipedia.org/wiki/Peter_Duesberg
[Manuela Gretkowska]: https://pl.wikipedia.org/wiki/Manuela_Gretkowska
[Mariusz Wilk]: https://pl.wikipedia.org/wiki/Mariusz_Wilk
[Jerzy Giedroyc]: https://pl.wikipedia.org/wiki/Jerzy_Giedroyc
[Kultura]: https://pl.wikipedia.org/wiki/Kultura_(miesięcznik)
[Nike]: https://pl.wikipedia.org/wiki/Nagroda_Literacka_%E2%80%9ENike%E2%80%9D
[Jan Drzeżdżon]: https://pl.wikipedia.org/wiki/Jan_Drzeżdżon
[Marek Słyk]: https://pl.wikipedia.org/wiki/Marek_Słyk
[Henryk Bereza]: https://pl.wikipedia.org/wiki/Henryk_Bereza
[Zaklęte rewiry]: https://pl.wikipedia.org/wiki/Zaklęte_rewiry_(powieść)
[Henryk Worcell]: https://pl.wikipedia.org/wiki/Henryk_Worcell
[Tomasz Fiałkowski]: https://pl.wikipedia.org/wiki/Tomasz_Fiałkowski
[Jerzy Jarzębski]: https://pl.wikipedia.org/wiki/Jerzy_Jarzębski
[stan wojenny]: https://pl.wikipedia.org/wiki/Stan_wojenny_w_Polsce_(1981%E2%80%931983)
[Berlin Zachodni]: https://pl.wikipedia.org/wiki/Berlin_Zachodni
[Microsoft]: https://pl.wikipedia.org/wiki/Microsoft
[Bill Gates]: https://pl.wikipedia.org/wiki/Bill_Gates
[Bill Clinton]: https://pl.wikipedia.org/wiki/Bill_Clinton
[Związek Radziecki]: https://pl.wikipedia.org/wiki/Związek_Radziecki
[Polska Ludowa]: https://pl.wikipedia.org/wiki/Polska_Ludowa
[Galaktyka Gutenberga]: https://pl.wikipedia.org/wiki/Galaktyka_Gutenberga
[Immanuel Kant]: https://pl.wikipedia.org/wiki/Immanuel_Kant
[Chartreuse]: https://pl.wikipedia.org/wiki/Chartreuse
[Jugosławia]: https://pl.wikipedia.org/wiki/Jugosławia
[Ernest Hemingway]: https://pl.wikipedia.org/wiki/Ernest_Hemingway
[bracia Strugaccy]: https://pl.wikipedia.org/wiki/Arkadij_i_Boris_Strugaccy
[Philip Dick]: https://pl.wikipedia.org/wiki/Philip_K._Dick
[Olaf Stapledon]: https://pl.wikipedia.org/wiki/Olaf_Stapledon
[Jerzy Żuławski]: https://pl.wikipedia.org/wiki/Jerzy_Żuławski
[Karol Maj]: https://pl.wikipedia.org/wiki/Karl_May
[Pitigrilli]: https://pl.wikipedia.org/wiki/Pitigrilli
[Stalin]: https://pl.wikipedia.org/wiki/Stalin
[Tomasz Lem]: https://pl.wikipedia.org/wiki/Tomasz_Lem
[Princeton]: https://pl.wikipedia.org/wiki/Uniwersytet_w_Princeton
[Andrzej Szczeklik]: https://pl.wikipedia.org/wiki/Andrzej_Szczeklik
[Georges Simenon]: https://pl.wikipedia.org/wiki/Georges_Simenon
[Politechnika Lwowska]: https://pl.wikipedia.org/wiki/Politechnika_Lwowska
[Homer]: https://pl.wikipedia.org/wiki/Homer
[Gilgamesz]: https://pl.wikipedia.org/wiki/Gilgamesz
[Joanna Chmielewska]: https://pl.wikipedia.org/wiki/Joanna_Chmielewska
[Maria Nurowska]: https://pl.wikipedia.org/wiki/Maria_Nurowska
[Stephen Hawking]: https://pl.wikipedia.org/wiki/Stephen_Hawking
